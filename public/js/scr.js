const API = "/App/index.php";

$(document).ready(function() {
    $( "#btn-back" ).click(function() {
        resetSearch();
        toggleSearchResult();
    });

    $( "#btn-submit" ).click(function() {
        var code = $("#code").val();
        var data = JSON.stringify({ "code": code });
        postForm(data)
    });
});

function postForm(data)
{
    $.post( API, data)
        .done(function( data ) {
            var response = jQuery.parseJSON(data);
            var prefix = response.prefix;
            var name = response.name;
            $("#prefix").val(prefix);
            $("#name").val(name);

            toggleSearchResult();
        });
}

function resetSearch()
{
    $("#prefix").val('');
    $("#name").val('');
    $("#code").val('');
}

function toggleSearchResult()
{
    $("#resultform").toggle();
    $("#searchform").toggle();
}

