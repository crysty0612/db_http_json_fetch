<?php
namespace App;

use App\CountryDetails\Database\CountryDetails;
use App\CountryDetails\Database\CountryFetcher;
use App\DataBase\Query\Conditions\WhereCondition;
use App\DataBase\Query\Select;

class CountryDbFetcher implements CountryFetcher
{
    public function getCountryByCode($code)
    {
        $select = new Select(
            self::COUNTRIES_TABLE,
            array(self::CODE_FIELD, self::PREFIX_FIELD, self::NAME_FIELD)
        );
        $select->where(WhereCondition::LIKE(self::CODE_FIELD, $code.'%'));

        $raw = $select->execute();
        $raw = $raw[0];

        return $this->mapRawCountryDataToCountryDetails($raw);
    }

    private function mapRawCountryDataToCountryDetails($raw)
    {
        if (empty($raw)) {
            return null;
        }

        return new CountryDetails($raw[self::CODE_FIELD], $raw[self::PREFIX_FIELD], $raw[self::NAME_FIELD]);
    }
}