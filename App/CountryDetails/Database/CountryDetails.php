<?php
namespace App\CountryDetails\Database;

class CountryDetails
{
    /** @var string $code */
    private $code;

    /** @var string $prefix */
    private $prefix;

    /** @var string $name */
    private $name;

    public function __construct($code, $prefix, $name)
    {
        $this->code = $code;
        $this->prefix = $prefix;
        $this->name = $name;
    }
    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}