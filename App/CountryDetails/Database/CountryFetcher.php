<?php
namespace App\CountryDetails\Database;

interface CountryFetcher
{
    const COUNTRIES_TABLE = 'locations_countries';
    const CODE_FIELD = 'code';
    const PREFIX_FIELD = 'prefix';
    const NAME_FIELD = 'name';

    /**
     * @param string $code
     * @return CountryDetails
     */
    public function getCountryByCode($code);
}