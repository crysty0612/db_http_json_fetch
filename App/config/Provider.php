<?php
namespace App\config;

interface Provider
{
    public function getConfig($configName);
}