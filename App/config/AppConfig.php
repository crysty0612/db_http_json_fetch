<?php
namespace App\config;

class AppConfig implements Config
{
    public static function getAppDbConfig()
    {
        return ConfigProvider::get();
    }
}