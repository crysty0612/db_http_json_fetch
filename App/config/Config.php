<?php
namespace App\config;


interface Config
{
    /** @return ConfigProvider */
    public static function getAppDbConfig();

}