<?php
namespace App\config;

class ConfigProvider implements Provider
{
    const PROP_FILE_SUPPORTED = 'properties';
    private static $instance;

    private function __construct(){}

    public static function get()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function getConfig($configName)
    {
        $file = $this->getPropertiesFile($configName);
        if (!file_exists($file))
        {
            throw new MissingConfigProperties('Missing properties file: '.$file);
        }
        $props = parse_ini_file($file);

        return ($props ? $props : null);
    }

    private function getPropertiesFile($configName)
    {
        return dirname(__FILE__).'/properties/'.$configName.'.'.self::PROP_FILE_SUPPORTED;
    }

}