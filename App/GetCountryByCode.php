<?php
namespace App;
require_once('../vendor/autoload.php');

use App\Request\JsonPost;
use App\Request\CountryByCodeValidator;
use App\Request\Validator\JsonRequestValidator;

class GetCountryByCode
{
    /** @var JsonRequestValidator */
    private $validator;

    public function exec()
    {
        $this->createValidator();
        $this->addRequestRules();

        $this->output($this->getCountryDetails());
    }

    private function output($output)
    {
        echo json_encode($output);
    }

    private function createValidator()
    {
        $this->validator = new CountryByCodeValidator();
    }

    private function addRequestRules()
    {
        $this->validator->addRequired('code');
        $this->validator->minLength('code', 2);
    }

    private function getCountryDetails()
    {
        $dbReader = new \App\CountryDbFetcher();
        $countryDetails = $dbReader->getCountryByCode($this->getRequestCode());

        return array(
            'prefix' => $countryDetails->getPrefix(),
            'name' => $countryDetails->getName()
        );
    }

    private function getRequestCode()
    {
        $request = new JsonPost($this->validator);
        $request = $request->getData();
        return $request['code'];
    }
}
