<?php
namespace App\Request\Validator;

interface JsonRequestValidator extends PostValidator
{
    public function addRequired($key);
    public function minLength($key, $min);
}