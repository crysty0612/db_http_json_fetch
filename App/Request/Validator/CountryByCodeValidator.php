<?php
namespace App\Request;

use App\Request\Validator\JsonRequestValidator;
use App\Request\Validator\Exception\InvalidRequest;
use App\Request\Validator\Exception\InvalidParameter;
use App\Request\Validator\Exception\MissingParameter;
use App\Request\Validator\PostRequestValidator;

class CountryByCodeValidator extends PostRequestValidator implements JsonRequestValidator
{
    private $required = array();
    private $minLength = array();
    private $data;

    public function addRequired($key)
    {
        $this->required[] = $key;
    }

    public function minLength($key, $min)
    {
        $this->minLength[$key] = $min;
    }

    /**
     * @param $data
     * @return bool
     * @throws InvalidParameter
     * @throws InvalidRequest
     * @throws MissingParameter
     */
    public function validate($data)
    {
        $this->valid = parent::validate();
        $this->data = $data;
        $this->validateJson();
        $this->validatePresence();
        $this->validateLength();

        return $this->valid;
    }

    private function validateJson()
    {
        $this->valid = $this->valid && ($this->data !== null);
        if (!$this->valid) {
            throw new InvalidRequest("Invalid json content");
        }
    }

    private function validatePresence()
    {
        foreach ($this->required as $item) {
            $this->valid = $this->valid && array_key_exists($item, $this->data);
            if (!$this->valid) {
                throw new MissingParameter("Missing $item");
            };
        }
    }

    private function validateLength()
    {
        foreach ($this->minLength as $key => $length) {

            $this->valid = $this->valid && isset($this->data[$key]) && strlen($this->data[$key]) >= $length;
            if (!$this->valid) {
                throw new InvalidParameter("Minimum length: $length for $key param");
            };
        }
    }
}