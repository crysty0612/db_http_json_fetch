<?php
namespace App\Request\Validator;

use App\Request\Validator\Exception\InvalidParameter;
use App\Request\Validator\Exception\InvalidRequest;
use App\Request\Validator\Exception\MissingParameter;

class PostRequestValidator implements PostValidator
{
    private $acceptedMethod;
    protected $valid = true;

    /**
     * @param $data
     * @return bool
     * @throws InvalidRequest
     * @throws InvalidParameter
     * @throws MissingParameter
     */
    public function validate($data)
    {
        $this->validateMethod();
        return $this->valid;
    }

    public function acceptedMethod($method)
    {
        $this->acceptedMethod = $method;
    }

    private function validateMethod()
    {
        $this->valid = $this->valid &&
            (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST');
        if (!$this->valid) {
            throw new InvalidRequest("Invalid requese method");
        }
    }
}