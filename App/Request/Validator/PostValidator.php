<?php

namespace App\Request\Validator;

use App\Validator\Validator;

interface PostValidator extends Validator
{
    public function acceptedMethod($method);
}