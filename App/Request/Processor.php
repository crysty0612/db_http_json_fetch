<?php
namespace App\Request;

use App\Validator\Validator;

abstract class Processor
{
    protected $paramValidator;

    public function __construct(Validator $validator)
    {
        $this->paramValidator = $validator;

        $this->parseRequest();
        $this->validateRequest();
    }

    protected abstract function parseRequest();
    protected abstract function validateRequest();

    protected function getBody()
    {
        return file_get_contents('php://input');
    }
}