<?php
namespace App\Request;

use App\Validator\Validator;

abstract class Post extends Processor
{
    public function __construct(Validator $validator)
    {
        $validator->acceptedMethod('POST');
        parent::__construct($validator);
    }
}