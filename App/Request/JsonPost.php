<?php
namespace App\Request;

class JsonPost extends Post
{
    private $requestBody;

    protected function parseRequest()
    {
        $this->requestBody = $this->getBody();
        $this->requestBody = json_decode($this->requestBody, true);
    }

    protected function validateRequest()
    {
        $this->paramValidator->validate($this->requestBody);
    }

    public function getData()
    {
        return $this->requestBody;
    }
}