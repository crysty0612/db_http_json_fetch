<?php
namespace App\Validator;

interface Validator
{
    /**
     * @param $data
     * @return bool
     */
    public function validate($data);
}