<?php
namespace App\DataBase\Query;

class Select extends SelectBuilder
{
    public function execute()
    {
        $this->prepareQuery();

        if ($this->sqlQuery->execute()) {
            $result = $this->sqlQuery->get_result();

            $resultArray = array();
            while ($row = $result->fetch_assoc()) {
                $resultArray[] = $row;
            }

            return $resultArray;
        }

        return null;
    }
}