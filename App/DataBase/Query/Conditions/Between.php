<?php
namespace App\DataBase\Query\Conditions;

class Between extends WhereCondition
{
    private $left;
    private $right;

    public function getLeft()
    {
        return $this->left;
    }

    public function getRight()
    {
        return $this->right;
    }

    protected function __construct($field, $value, $condition)
    {
        parent::__construct($field, $value, $condition);

        $this->left = $value[0];
        $this->right = $value[1];
    }

    public function __toString()
    {
        return $this->field . ' ' . $this->condition . ' (?, ?)';
    }
}