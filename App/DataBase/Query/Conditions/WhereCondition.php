<?php
namespace App\DataBase\Query\Conditions;

class WhereCondition implements Condition
{
    protected $field;
    protected $value;
    protected $condition;

    protected function __construct($field, $value, $condition)
    {
        $this->field = $field;
        $this->value = $value;
        $this->condition = $condition;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getCondition()
    {
        return $this->condition;
    }

    public static function equals($field, $value)
    {
        return new self($field, $value, '=');
    }

    public static function notEquals($field, $value)
    {
        return new self($field, $value, '!=');
    }

    public static function less($field, $value)
    {
        return new self($field, $value, '<');
    }

    public static function greater($field, $value)
    {

        return new self($field, $value, '<');
    }

    public static function lessOrEqual($field, $value)
    {
        return new self($field, $value, '<');
    }

    public static function greaterOrEqual($field, $value)
    {
        return new self($field, $value, '<');
    }

    public static function like($field, $value)
    {
        return new self($field, $value, 'LIKE');
    }

    public static function between($field, $left, $right)
    {
        $value = array($left, $right);
        return new Between($field, $value, 'BETWEEN');
    }

    public static function in($field, array $list)
    {
        return new In($field, $list, 'IN');
    }

    public static function null($field)
    {
        return new Null($field, null, 'IS NULL');
    }

    public static function notNull($field)
    {
        return new Null($field, null, 'IS NOT NULL');
    }

    public function __toString()
    {
        return $this->field . ' ' . $this->condition . ' ?';
    }
}