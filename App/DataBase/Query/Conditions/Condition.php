<?php
namespace App\DataBase\Query\Conditions;

interface Condition
{
    public function getValue();
    public function getCondition();

    /**
     * @param $field
     * @param $value
     * @return Condition
     */
    public static function equals($field, $value);
    /**
     * @param $field
     * @param $value
     * @return Condition
     */
    public static function notEquals($field, $value);
    /**
     * @param $field
     * @param $value
     * @return Condition
     */
    public static function less($field, $value);
    /**
     * @param $field
     * @param $value
     * @return Condition
     */
    public static function greater($field, $value);
    /**
     * @param $field
     * @param $value
     * @return Condition
     */
    public static function lessOrEqual($field, $value);
    /**
     * @param $field
     * @param $value
     * @return Condition
     */
    public static function greaterOrEqual($field, $value);
    /**
     * @param $field
     * @param $value
     * @return Condition
     */
    public static function like($field, $value);
    /**
     * @param $field
     * @param $left
     * @param $right
     * @return Condition
     */
    public static function between($field, $left, $right);
    /**
     * @param $field
     * @param $list
     * @return Condition
     */
    public static function in($field, array $list);
    /**
     * @param $field
     * @return Condition
     */
    public static function null($field);
    /**
     * @param $field
     * @return Condition
     */
    public static function notNull($field);

    /**
     * @return string
     */
    public function __toString();
}