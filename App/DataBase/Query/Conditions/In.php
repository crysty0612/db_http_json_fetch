<?php
namespace App\DataBase\Query\Conditions;

class In extends WhereCondition
{
    public function __toString()
    {
        return $this->field . ' ' . $this->condition;
    }
}