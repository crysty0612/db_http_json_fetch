<?php
namespace App\DataBase\Query;

use App\config\AppConfig;
use App\DataBase\Connection\ConnectionConfig;
use App\DataBase\Connection\Link;

abstract class QueryBuilder
{
    /** @var \mysqli  */
    protected $connection;
    /** @var \mysqli_stmt */
    protected $sqlQuery;

    protected $tableName;
    protected $keys;
    protected $values;

    public function __construct()
    {
        $this->connection = Link::get($this->getDbConfig());
    }

    private function getDbConfig()
    {
        return new ConnectionConfig(AppConfig::getAppDbConfig());
    }

    protected abstract function execute();
}