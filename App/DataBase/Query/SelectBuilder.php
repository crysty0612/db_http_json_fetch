<?php
namespace App\DataBase\Query;

use App\DataBase\Query\Conditions\Condition;

abstract class SelectBuilder extends QueryBuilder
{
    const DEFAULT_FIELDS_FETCH = '*';
    const FETCH_PLACEHOLDER = '%fields%';
    const TABLE_PLACEHOLDER = '%table%';
    const CONDITION_PLACEHOLDER = '%conditions%';

    /** @var Condition[] $where */
    protected $where;
    protected $join;

    public function __construct($table, $fields)
    {
        parent::__construct();
        $this->tableName = $table;
        $this->keys = $fields;
    }

    public function join($table, $leftMatch, $rightMatch, $alias = null)
    {
        $alias = ($alias ? ' AS ' . $alias : null);
        $this->join .= " JOIN $table $alias ON $leftMatch = $rightMatch";

        return $this;
    }

    public function where(Condition $condition)
    {
        $this->where[] = $condition;
        return $this;
    }

    protected function prepareQuery()
    {
        $sql = 'SELECT ' . $this->getFieldsString() .
            ' FROM ' . $this->tableName;

        $sql = $this->appendConditionsFields($sql);
        $sql = $this->appendJoins($sql);

        $this->sqlQuery = $this->connection->prepare($sql);

        $this->appendConditionsValues();
    }

    private function appendConditionsFields($sql)
    {
        $first = true;
        if (!empty($this->where)) {
            foreach ($this->where as $condition) {
                if ($first) {
                    $sql .= ' WHERE ' . $condition;
                }
                else {
                    $sql .= ' AND ' . $condition;
                }
                $first = false;
            }
        }

        return $sql;
    }

    private function appendJoins($sql)
    {
        if (strlen($this->join)) {
            $sql .= $this->join;
        }
    }

    private function appendConditionsValues()
    {
        $literals = '';
        $values = array();
        if (empty($this->where)) {
            return;
        }
        foreach ($this->where as $condition) {
            $literals .= $this->getParamLiteral($condition->getValue());
            $values[] = $condition->getValue();
        }

        $this->sqlQuery->bind_param($literals, ...$values);
    }

    private function getParamLiteral($value)
    {
        switch(gettype($value)) {
            case "integer": return 'i';
            case "double": return 'd';
            case "string": return 's';
            default: return null;
        }
    }

    private function getFieldsString()
    {
        return (is_array($this->keys) ? implode(', ', $this->keys) : self::DEFAULT_FIELDS_FETCH);
    }
}