<?php

namespace App\DataBase\Connection;

use App\config\Provider;

class ConnectionConfig implements Config
{
    const CONFIG_PROPS_NAME = 'testdb';
    const PROP_KEY_HOST = 'host';
    const PROP_KEY_USER = 'user';
    const PROP_KEY_PASSWORD = 'password';
    const PROP_KEY_DATABASE = 'name';

    /** @var Provider $configProvider */
    private $configProvider;

    private $host;
    private $user;
    private $password;
    private $database;

    public function __construct(Provider $configProvider)
    {
        $this->configProvider = $configProvider;
        $this->populateConfig();
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    private function populateConfig()
    {
        $rawProps = $this->configProvider->getConfig(self::CONFIG_PROPS_NAME);

        if (!isset($rawProps[self::PROP_KEY_HOST])) {
            throw new InvalidConfigDetails();
        }
        if (!isset($rawProps[self::PROP_KEY_USER])) {
            throw new InvalidConfigDetails();
        }
        if (!isset($rawProps[self::PROP_KEY_PASSWORD])) {
            throw new InvalidConfigDetails();
        }
        if (!isset($rawProps[self::PROP_KEY_DATABASE])) {
            throw new InvalidConfigDetails();
        }

        $this->host = $rawProps[self::PROP_KEY_HOST];
        $this->user = $rawProps[self::PROP_KEY_USER];
        $this->password = $rawProps[self::PROP_KEY_PASSWORD];
        $this->database = $rawProps[self::PROP_KEY_DATABASE];
    }
}