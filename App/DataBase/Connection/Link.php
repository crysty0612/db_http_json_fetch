<?php

namespace App\DataBase\Connection;

use \mysqli;

class Link implements Connection
{
    private static $connection;

    private function __construct(Config $config)
    {
        self::$connection = new mysqli(
            $config->getHost(),
            $config->getUser(),
            $config->getPassword(),
            $config->getDatabase()
        );
    }

    public static function get(Config $config)
    {
        if (!isset(self::$connection)) {
            new self($config);
        }

        return self::$connection;
    }
}