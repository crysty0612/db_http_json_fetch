<?php

namespace App\DataBase\Connection;

interface Config
{
    public function getHost();

    public function getUser();

    public function getPassword();

    public function getDatabase();
}